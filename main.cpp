#include <string>
#include <vector>
#include <filesystem>
#include "commandline.h"
#include "pugixml.hpp"
#include "binbuf.h"
#include "utils.h"
#include <stack>

using namespace std;
using namespace std::filesystem;

char const *VERSION = "1.2";

string BASE_MODULE;
unsigned int BASE_ADDRESS = 0;

enum class SceneLayout {
    Unknown,
    FIFA2003,
    FIFA2004,
    FIFA2005,
    CL0405,
    FIFA06
};

SceneLayout LAYOUT = SceneLayout::Unknown;

string ACCESSOR_COMMENT(int index) {
    if (LAYOUT != SceneLayout::FIFA06 || BASE_ADDRESS == 0 || index == -1)
        return string();
    static char hex[32];
    sprintf_s(hex, "%X", BASE_ADDRESS + index * 4);
    string result = " <!-- ";
    if (!BASE_MODULE.empty())
        result += BASE_MODULE + " ";
    result += "@";
    result += hex;
    result += " -->";
    return result;
}

string ReadString(FILE *f) {
    string result;
    char c = 0;
    fread(&c, 1, 1, f);
    while (c) {
        result += c;
        fread(&c, 1, 1, f);
    }
    return result;
}

void decompileScriptBin(path const &in, path const &out) {
    FILE *fin = _wfopen(in.c_str(), L"rb");
    if (fin) {
        FILE *fout = _wfopen(out.c_str(), L"wt");
        if (fout) {
            unsigned short version = 0;
            fread(&version, 2, 1, fin);
            fprintf(fout, "<Script Version=\"%d\"", version);
            if (version == 4) {
                unsigned short count = 0;
                fread(&count, 2, 1, fin);
                if (count > 0) {
                    fputs(">\n", fout);
                    unsigned short numStrings = 0;
                    fread(&numStrings, 2, 1, fin);
                    unsigned short padding = 0;
                    fread(&padding, 2, 1, fin);
                    auto entriesPos = ftell(fin);
                    vector<string> strings(numStrings);
                    auto GetString = [&](unsigned short stringIndex) -> string {
                        if (stringIndex < strings.size())
                            return strings[stringIndex];
                        return Format("ERROR STRING RESOLVING (%d)", stringIndex);
                    };
                    auto GetType = [](unsigned short typeId) -> string {
                        switch (typeId) {
                        case 0:
                            return "DEBUG";
                        case 1:
                            return "INTRO";
                        case 2:
                            return "END_HALF";
                        case 4:
                            return "END_GAME";
                        case 5:
                            return "GOAL";
                        case 6:
                            return "CARD";
                        case 7:
                            return "SUBSTITUTION";
                        case 8:
                            return "STREAKER";
                        case 9:
                            return "CORNER";
                        case 12:
                            return "P_KICK";
                        case 14:
                            return "THROW_IN";
                        case 15:
                            return "FOUL";
                        case 16:
                            return "MISS_CLOSE";
                        case 17:
                            return "MISS_WIDE";
                        case 18:
                            return "THROW_IN_NEAR";
                        case 19:
                            return "CORNER_BALL_OUT";
                        case 20:
                            return "MANAGER";
                        }
                        return Format("UNKNOWN_TYPE_%d", typeId);
                    };
                    for (unsigned int e = 0; e < count; e++) {
                        fseek(fin, 10, SEEK_CUR);
                        unsigned short dataSize = 0;
                        fread(&dataSize, 2, 1, fin);
                        if (dataSize > 0)
                            fseek(fin, dataSize, SEEK_CUR);
                    }
                    for (unsigned int s = 0; s < numStrings; s++)
                        strings[s] = ReadString(fin);
                    fseek(fin, entriesPos, SEEK_SET);
                    for (unsigned int e = 0; e < count; e++) {
                        unsigned short nameIndex = 0;
                        fread(&nameIndex, 2, 1, fin);
                        unsigned short index = 0;
                        fread(&index, 2, 1, fin);
                        unsigned short type = 0;
                        fread(&type, 2, 1, fin);
                        unsigned short variation = 0;
                        fread(&variation, 2, 1, fin);
                        unsigned short unk = 0;
                        fread(&unk, 2, 1, fin);
                        unsigned short dataSize = 0;
                        fread(&dataSize, 2, 1, fin);
                        fprintf(fout, "    <ScriptEntry EntryName=\"%s\" EntryType=\"%s\" EntryVariation=\"%d\" EntryU1=\"%d\"",
                            GetString(nameIndex).c_str(), GetType(type).c_str(), variation, unk);
                        if (dataSize > 0) {
                            unsigned short blocksCount = dataSize / 12;
                            fputs(">\n", fout);
                            stack<unsigned short> childCountStack;
                            bool first = true;
                            for (unsigned short b = 0; b < blocksCount; b++) {
                                fputs("        ", fout);
                                for (unsigned int cc = 0; cc < childCountStack.size(); cc++)
                                    fputs("    ", fout);
                                if (!childCountStack.empty())
                                    childCountStack.top()--;
                                unsigned int blockType = 0;
                                fread(&blockType, 4, 1, fin);
                                if (blockType != 0 && blockType != 1 && blockType != 10) {
                                    unsigned int priority = 0;
                                    fread(&priority, 4, 1, fin);
                                    unsigned int numChilds = 0;
                                    fread(&numChilds, 4, 1, fin);
                                    fprintf(fout, "<Block BlockType=\"%d\" Param1=\"%d\"", blockType, priority);
                                    if (numChilds > 0) {
                                        fputs(">\n", fout);
                                        childCountStack.push(numChilds);
                                    }
                                    else
                                        fputs("/>\n", fout);
                                }
                                else {
                                    unsigned short p0 = 0;
                                    fread(&p0, 2, 1, fin);
                                    unsigned short p1 = 0;
                                    fread(&p1, 2, 1, fin);
                                    unsigned short p2 = 0;
                                    fread(&p2, 2, 1, fin);
                                    unsigned short p3 = 0;
                                    fread(&p3, 2, 1, fin);
                                    if (blockType == 0)
                                        fprintf(fout, "<Block BlockType=\"%d\" Param1=\"%d\" Param2=\"%d\" Param3=\"%d\" Param4=\"%d\"/>\n", blockType, p0, p1, p2, p3);
                                    else if (blockType == 1)
                                        fprintf(fout, "<Block BlockType=\"%d\" AnimName=\"%s\" TimeStart=\"%d\" TimeEnd=\"%d Priority=\"%d\"/>\n", blockType, GetString(p0).c_str(), p1, p2, p3);
                                    else
                                        fprintf(fout, "<Block BlockType=\"%d\" Param1=\"%d\" Param2=\"%d\" Param3=\"%d\" Param4=\"%d\"/>\n", blockType, p0, p1, p2, p3);
                                }
                                while (!childCountStack.empty() && childCountStack.top() == 0) {
                                    childCountStack.pop();
                                    fputs("        ", fout);
                                    for (unsigned int cc = 0; cc < childCountStack.size(); cc++)
                                        fputs("    ", fout);
                                    fputs("</Block>\n", fout);
                                }
                                first = false;
                            }
                            fputs("    </ScriptEntry>\n", fout);
                        }
                        else
                            fputs("/>\n", fout);
                    }
                    fputs("</Script>\n", fout);
                }
                else
                    fputs("/>\n", fout);
            }
        }
    }
}

void decompile(path const &in, path const &out) {
    FILE *fin = _wfopen(in.c_str(), L"rb");
    if (fin) {
        FILE *fout = _wfopen(out.c_str(), L"wt");
        if (fout) {
            unsigned int numDataTypes = 0;
            unsigned int headerSize = 0;
            unsigned int sceneEntrySize = 0;
            fputs("<SceneFile>\n    <Layout Type=\"", fout);
            unsigned int value = 0;
            fread(&value, 4, 1, fin);
            if (value == 'MSGS') {
                fread(&numDataTypes, 4, 1, fin);
                fread(&headerSize, 4, 1, fin);
                fread(&sceneEntrySize, 4, 1, fin);
                if (numDataTypes == 13) {
                    if (headerSize == 112 && sceneEntrySize == 108) {
                        unsigned int sizes[11];
                        fread(sizes, 4, 11, fin);
                        if (sizes[0] == 20 && sizes[1] == 8 && sizes[2] == 8 && sizes[3] == 24 && sizes[4] == 12 && sizes[5] == 20
                            && sizes[6] == 88 && sizes[7] == 4 && sizes[8] == 12 && sizes[9] == 4 && sizes[10] == 8)
                        {
                            LAYOUT = SceneLayout::FIFA06;
                            fputs("FIFA06", fout);
                        }
                    }
                }
                else if (numDataTypes == 11) {
                    if (headerSize == 112) {
                        if (sceneEntrySize == 108) {
                            unsigned int sizes[11];
                            fread(sizes, 4, 11, fin);
                            if (sizes[0] == 20 && sizes[1] == 8 && sizes[2] == 4 && sizes[3] == 8 && sizes[4] == 24 && sizes[5] == 12
                                && sizes[6] == 20 && sizes[7] == 88 && sizes[8] == 12 && sizes[9] == 8 && sizes[10] == 0xFFFFFFFF)
                            {
                                LAYOUT = SceneLayout::CL0405;
                                fputs("CL0405", fout);
                            }
                        }
                    }
                    else if (headerSize == 96) {
                        if (sceneEntrySize == 108) {
                            unsigned int sizes[9];
                            fread(sizes, 4, 9, fin);
                            if (sizes[0] == 20 && sizes[1] == 8 && sizes[2] == 8 && sizes[3] == 24 && sizes[4] == 12
                                && sizes[5] == 20 && sizes[6] == 88 && sizes[7] == 12 && sizes[8] == 8)
                            {
                                LAYOUT = SceneLayout::FIFA2005;
                                fputs("FIFA2005", fout);
                            }
                        }
                        else if (sceneEntrySize == 104) {
                            unsigned int sizes[9];
                            fread(sizes, 4, 9, fin);
                            if (sizes[0] == 20 && sizes[1] == 8 && sizes[2] == 8 && sizes[3] == 24 && sizes[4] == 12
                                && sizes[5] == 20 && sizes[6] == 80 && sizes[7] == 12 && sizes[8] == 8)
                            {
                                LAYOUT = SceneLayout::FIFA2004;
                                fputs("FIFA2004", fout);
                            }
                        }
                    }
                    else if (headerSize == 100) {
                        if (sceneEntrySize == 104) {
                            unsigned int sizes[10];
                            fread(sizes, 4, 10, fin);
                            if (sizes[0] == 20 && sizes[1] == 8 && sizes[2] == 8 && sizes[3] == 24 && sizes[4] == 12
                                && sizes[5] == 20 && sizes[6] == 84 && sizes[7] == 12 && sizes[8] == 8 && sizes[9] == 24)
                            {
                                LAYOUT = SceneLayout::FIFA2003;
                                fputs("FIFA2003", fout);
                            }
                        }
                    }
                }
            }
            if (LAYOUT == SceneLayout::Unknown)
                fprintf(fout, "UNKNOWN\" Version=\"%s\"></Layout>\n", VERSION);
            else {
                fread(&value, 4, 1, fin);
                unsigned int numStrings = 0;
                unsigned int stringsOffset = 0;
                if (LAYOUT != SceneLayout::FIFA06) {
                    fread(&numStrings, 4, 1, fin);
                    fread(&stringsOffset, 4, 1, fin);
                }
                unsigned int numEntries = 0;
                fread(&numEntries, 4, 1, fin);
                unsigned int entriesOffset = 0;
                fread(&entriesOffset, 4, 1, fin);
                unsigned int numRenderSlots = 0;
                fread(&numRenderSlots, 4, 1, fin);
                fprintf(fout, "\" NumRenderSlots=\"%d\" Version=\"%s\"></Layout>\n", numRenderSlots, VERSION);
                unsigned int numParameters = 0;
                fread(&numParameters, 4, 1, fin);
                unsigned int parametersOffset = 0;
                fread(&parametersOffset, 4, 1, fin);
                unsigned int numLoaders = 0;
                fread(&numLoaders, 4, 1, fin);
                unsigned int loadersOffset = 0;
                fread(&loadersOffset, 4, 1, fin);
                vector<string> sceneEntryName;
                vector<string> layerNames;
                vector<string> variableNames;
                vector<string> strings;
                map<unsigned int, string> accessorNames;
                unsigned int numUnkEntries2 = 0;
                unsigned int unkEntries2Offset = 0;
                if (LAYOUT == SceneLayout::CL0405) {
                    fread(&numUnkEntries2, 4, 1, fin);
                    fread(&unkEntries2Offset, 4, 1, fin);
                }
                else if (LAYOUT == SceneLayout::FIFA06) {
                    unsigned int numLayerNames = 0;
                    fread(&numLayerNames, 4, 1, fin);
                    unsigned int layerNamesOffset = 0;
                    fread(&layerNamesOffset, 4, 1, fin);
                    unsigned int numVariableNames = 0;
                    fread(&numVariableNames, 4, 1, fin);
                    unsigned int variableNamesOffset = 0;
                    fread(&variableNamesOffset, 4, 1, fin);
                    if (numLayerNames > 0) {
                        layerNames.resize(numLayerNames);
                        fseek(fin, layerNamesOffset, SEEK_SET);
                        for (unsigned int i = 0; i < numLayerNames; i++) {
                            unsigned int nameOffset = 0;
                            fread(&nameOffset, 4, 1, fin);
                            auto pos = ftell(fin);
                            fseek(fin, nameOffset, SEEK_SET);
                            layerNames[i] = ReadString(fin);
                            fseek(fin, pos, SEEK_SET);
                        }
                    }
                    if (numVariableNames > 0) {
                        variableNames.resize(numVariableNames);
                        fseek(fin, variableNamesOffset, SEEK_SET);
                        for (unsigned int i = 0; i < numVariableNames; i++) {
                            unsigned int nameOffset = 0;
                            fread(&nameOffset, 4, 1, fin);
                            auto pos = ftell(fin);
                            fseek(fin, nameOffset, SEEK_SET);
                            variableNames[i] = ReadString(fin);
                            fseek(fin, pos, SEEK_SET);
                        }
                    }
                }

                if (numStrings > 0) {
                    strings.resize(numStrings);
                    fseek(fin, stringsOffset, SEEK_SET);
                    for (unsigned int i = 0; i < numStrings; i++) {
                        strings[i] = ReadString(fin);
                        auto slen = strings[i].size() + 1;
                        if ((slen % 4) != 0)
                            fseek(fin, 4 - (slen % 4), SEEK_CUR);
                    }
                }

                auto ACCESSOR_PTR = [&](int index) {
                    if (index == -1)
                        return string("NULL");
                    if (LAYOUT == SceneLayout::FIFA06)
                        return to_string(index);
                    if (index >= strings.size())
                        return string("ERROR_ACCESSOR_NAME_RESOLVING");
                    return strings[index];
                };

                if (numEntries > 0) {
                    sceneEntryName.resize(numEntries);
                    fseek(fin, entriesOffset, SEEK_SET);
                    fputs("    <SceneEntries>\n", fout);
                    for (unsigned int i = 0; i < numEntries; i++) {
                        int objectIdAccessor = 0;
                        fread(&objectIdAccessor, 4, 1, fin);
                        unsigned int objectId = 0;
                        fread(&objectId, 4, 1, fin);
                        unsigned int switchable = 0;
                        fread(&switchable, 4, 1, fin);
                        int drawStatusAccessor = 0;
                        fread(&drawStatusAccessor, 4, 1, fin);
                        unsigned int drawStatus = 0;
                        fread(&drawStatus, 4, 1, fin);
                        int poseAccessor = 0;
                        fread(&poseAccessor, 4, 1, fin);
                        int transformAccessor = 0;
                        fread(&transformAccessor, 4, 1, fin);
                        unsigned int boundingUsed = 0;
                        fread(&boundingUsed, 4, 1, fin);
                        float bounding_x = 0.0f;
                        fread(&bounding_x, 4, 1, fin);
                        float bounding_y = 0.0f;
                        fread(&bounding_y, 4, 1, fin);
                        float bounding_z = 0.0f;
                        fread(&bounding_z, 4, 1, fin);
                        float bounding_r = 0.0f;
                        fread(&bounding_r, 4, 1, fin);
                        int ps2kValueAccessor = 0;
                        fread(&ps2kValueAccessor, 4, 1, fin);
                        unsigned int u10 = 0;
                        fread(&u10, 4, 1, fin);
                        unsigned int numTexBlocks = 0;
                        fread(&numTexBlocks, 4, 1, fin);
                        unsigned int texBlocksOffset = 0;
                        fread(&texBlocksOffset, 4, 1, fin);
                        unsigned int numModelBlocks = 0;
                        fread(&numModelBlocks, 4, 1, fin);
                        unsigned int modelBlocksOffset = 0;
                        fread(&modelBlocksOffset, 4, 1, fin);
                        int initAccessor = 0;
                        fread(&initAccessor, 4, 1, fin);
                        int renderAccessor = 0;
                        fread(&renderAccessor, 4, 1, fin);
                        unsigned int u11 = 0;
                        fread(&u11, 4, 1, fin);
                        int purgeAccessor = 0;
                        fread(&purgeAccessor, 4, 1, fin);
                        int object = 0;
                        fread(&object, 4, 1, fin);
                        unsigned int pool = 0;
                        fread(&pool, 4, 1, fin);
                        unsigned int poolNameOffset = 0;
                        if (sceneEntrySize != 104)
                            fread(&poolNameOffset, 4, 1, fin);
                        unsigned int nameOffset = 0;
                        fread(&nameOffset, 4, 1, fin);
                        unsigned int u12 = 0;
                        fread(&u12, 4, 1, fin);
                        auto pos = ftell(fin);
                        fseek(fin, nameOffset, SEEK_SET);
                        string name = ReadString(fin);
                        sceneEntryName[i] = name;
                        string poolName;
                        if (sceneEntrySize != 104) {
                            fseek(fin, poolNameOffset, SEEK_SET);
                            poolName = ReadString(fin);
                        }
                        if (sceneEntrySize != 104) {
                            if (boundingUsed)
                                fprintf(fout, "        <SceneEntry Name=\"%s\" PoolName=\"%s\" ObjectId=\"%d\" BoundingCenter=\"%g,%g,%g\" BoundingRadius=\"%g\" RenderSlot=\"%d\">\n", name.c_str(), poolName.c_str(), objectId, bounding_x, bounding_y, bounding_z, bounding_r, u11);
                            else
                                fprintf(fout, "        <SceneEntry Name=\"%s\" PoolName=\"%s\" ObjectId=\"%d\" RenderSlot=\"%d\">\n", name.c_str(), poolName.c_str(), objectId, u11);
                        }
                        else {
                            if (boundingUsed)
                                fprintf(fout, "        <SceneEntry Name=\"%s\" ObjectId=\"%d\" BoundingCenter=\"%g,%g,%g\" BoundingRadius=\"%g\" RenderSlot=\"%d\">\n", name.c_str(), objectId, bounding_x, bounding_y, bounding_z, bounding_r, u11);
                            else
                                fprintf(fout, "        <SceneEntry Name=\"%s\" ObjectId=\"%d\" RenderSlot=\"%d\">\n", name.c_str(), objectId, u11);
                        }
                        if (drawStatusAccessor != -1)
                            fprintf(fout, "            <DrawStatusAccessor>%s</DrawStatusAccessor>%s\n", ACCESSOR_PTR(drawStatusAccessor).c_str(), ACCESSOR_COMMENT(drawStatusAccessor).c_str());
                        if (transformAccessor != -1)
                            fprintf(fout, "            <TransformAccessor>%s</TransformAccessor>%s\n", ACCESSOR_PTR(transformAccessor).c_str(), ACCESSOR_COMMENT(transformAccessor).c_str());
                        if (object != -1)
                            fprintf(fout, "            <ObjectDataAccessor>%s</ObjectDataAccessor>%s\n", ACCESSOR_PTR(object).c_str(), ACCESSOR_COMMENT(object).c_str());
                        if (initAccessor != -1)
                            fprintf(fout, "            <InitAccessor>%s</InitAccessor>%s\n", ACCESSOR_PTR(initAccessor).c_str(), ACCESSOR_COMMENT(initAccessor).c_str());
                        if (renderAccessor != -1)
                            fprintf(fout, "            <PreRenderAccessor>%s</PreRenderAccessor>%s\n", ACCESSOR_PTR(renderAccessor).c_str(), ACCESSOR_COMMENT(renderAccessor).c_str());
                        if (purgeAccessor != -1)
                            fprintf(fout, "            <PurgeAccessor>%s</PurgeAccessor>%s\n", ACCESSOR_PTR(purgeAccessor).c_str(), ACCESSOR_COMMENT(purgeAccessor).c_str());
                        if (objectIdAccessor != -1)
                            fprintf(fout, "            <ObjectIdAccessor>%s</ObjectIdAccessor>%s\n", ACCESSOR_PTR(objectIdAccessor).c_str(), ACCESSOR_COMMENT(objectIdAccessor).c_str());
                        if (poseAccessor != -1)
                            fprintf(fout, "            <PoseAccessor>%s</PoseAccessor>%s\n", ACCESSOR_PTR(poseAccessor).c_str(), ACCESSOR_COMMENT(poseAccessor).c_str());
                        if (ps2kValueAccessor != -1)
                            fprintf(fout, "            <KValueAccessor>%s</KValueAccessor>%s\n", ACCESSOR_PTR(ps2kValueAccessor).c_str(), ACCESSOR_COMMENT(ps2kValueAccessor).c_str());
                        if (numTexBlocks > 0) {
                            fseek(fin, texBlocksOffset, SEEK_SET);
                            fputs("            <TexBlocks>\n", fout);
                            for (unsigned int n = 0; n < numTexBlocks; n++) {
                                unsigned int numTextures = 0;
                                fread(&numTextures, 4, 1, fin);
                                unsigned int texturesOffset = 0;
                                fread(&texturesOffset, 4, 1, fin);
                                unsigned int numTexAssignments = 0;
                                fread(&numTexAssignments, 4, 1, fin);
                                unsigned int texAssignmentsOffset = 0;
                                fread(&texAssignmentsOffset, 4, 1, fin);
                                unsigned int texBlockNameOffset = 0;
                                fread(&texBlockNameOffset, 4, 1, fin);
                                unsigned int tb_u1 = 0;
                                fread(&tb_u1, 4, 1, fin);
                                auto tb_pos = ftell(fin);
                                fseek(fin, texBlockNameOffset, SEEK_SET);
                                string tb_name = ReadString(fin);
                                fprintf(fout, "                <TexBlock Name=\"%s\">", tb_name.c_str());
                                if (numTextures > 0 || numTexAssignments > 0)
                                    fputs("\n", fout);
                                if (numTextures > 0) {
                                    fputs("                    <Textures>\n", fout);
                                    fseek(fin, texturesOffset, SEEK_SET);
                                    for (unsigned int t = 0; t < numTextures; t++) {
                                        unsigned int texNameOffset = 0;
                                        fread(&texNameOffset, 4, 1, fin);
                                        unsigned int texNumArguments = 0;
                                        fread(&texNumArguments, 4, 1, fin);
                                        unsigned int texArgumentsOffset = 0;
                                        fread(&texArgumentsOffset, 4, 1, fin);
                                        auto texPos = ftell(fin);
                                        string texName;
                                        if (texNameOffset) {
                                            fseek(fin, texNameOffset, SEEK_SET);
                                            texName = ReadString(fin);
                                        }
                                        else
                                            texName = "(null)";
                                        fprintf(fout, "                        <Texture NameFormat=\"%s\">", texName.c_str());
                                        if (texNumArguments > 0) {
                                            fputs("\n                            <FormatArguments>\n", fout);
                                            fseek(fin, texArgumentsOffset, SEEK_SET);
                                            for (unsigned int a = 0; a < texNumArguments; a++) {
                                                int texFormatArgumentAccessor = 0;
                                                fread(&texFormatArgumentAccessor, 4, 1, fin);
                                                fread(&value, 4, 1, fin);
                                                fprintf(fout, "                                <FormatArgumentAccessor>%s</FormatArgumentAccessor>%s\n", ACCESSOR_PTR(texFormatArgumentAccessor).c_str(), ACCESSOR_COMMENT(texFormatArgumentAccessor).c_str());
                                            }
                                            fputs("                            </FormatArguments>\n                        ", fout);
                                        }
                                        fputs("</Texture>\n", fout);
                                        fseek(fin, texPos, SEEK_SET);
                                    }
                                    fputs("                    </Textures>", fout);
                                }
                                if (numTexAssignments > 0) {
                                    if (numTextures > 0)
                                        fputs("\n", fout);
                                    fputs("                    <TextureAssignments>\n", fout);
                                    fseek(fin, texAssignmentsOffset, SEEK_SET);
                                    for (unsigned int t = 0; t < numTexAssignments; t++) {
                                        char targetTag[5];
                                        fread(targetTag, 1, 4, fin);
                                        reverse(&targetTag[0], &targetTag[4]);
                                        targetTag[4] = 0;
                                        int sourceTag = 0;
                                        fread(&sourceTag, 4, 1, fin);
                                        unsigned int ta_u1 = 0;
                                        fread(&ta_u1, 4, 1, fin);
                                        unsigned int ta_u2 = 0;
                                        fread(&ta_u2, 4, 1, fin);
                                        unsigned int ta_u3 = 0;
                                        fread(&ta_u3, 4, 1, fin);
                                        fprintf(fout, "                        <TextureAssignment TargetTag=\"%s\">\n", targetTag);
                                        fprintf(fout, "                            <SourceTagAccessor>%s</SourceTagAccessor>%s\n", ACCESSOR_PTR(sourceTag).c_str(), ACCESSOR_COMMENT(sourceTag).c_str());
                                        fputs("                        </TextureAssignment>\n", fout);
                                    }
                                    fputs("                    </TextureAssignments>", fout);
                                }
                                if (numTextures > 0 || numTexAssignments > 0)
                                    fputs("\n                ", fout);
                                fputs("</TexBlock>\n", fout);
                                fseek(fin, tb_pos, SEEK_SET);
                            }
                            fputs("            </TexBlocks>\n", fout);
                        }
                        if (numModelBlocks > 0) {
                            fseek(fin, modelBlocksOffset, SEEK_SET);
                            fputs("            <ModelBlocks>\n", fout);
                            for (unsigned int n = 0; n < numModelBlocks; n++) {
                                unsigned int mb_u1 = 0;
                                fread(&mb_u1, 4, 1, fin);
                                unsigned int modelNameOffset = 0;
                                fread(&modelNameOffset, 4, 1, fin);
                                int nearClipAccessor = 0;
                                fread(&nearClipAccessor, 4, 1, fin);
                                float nearClip = 0;
                                fread(&nearClip, 4, 1, fin);
                                int farClipAccessor = 0;
                                fread(&farClipAccessor, 4, 1, fin);
                                float farClip = 0;
                                fread(&farClip, 4, 1, fin);
                                unsigned int mb_u2 = 0;
                                fread(&mb_u2, 4, 1, fin);
                                unsigned int mb_u3 = 0;
                                fread(&mb_u3, 4, 1, fin);
                                int fifaId = 0;
                                int morphInfo = 0;
                                if (LAYOUT == SceneLayout::FIFA06 || LAYOUT == SceneLayout::CL0405 || LAYOUT == SceneLayout::FIFA2005) {
                                    fread(&fifaId, 4, 1, fin);
                                    fread(&morphInfo, 4, 1, fin);
                                }
                                unsigned int modelNumArguments = 0;
                                fread(&modelNumArguments, 4, 1, fin);
                                unsigned int modelArgumentsOffset = 0;
                                fread(&modelArgumentsOffset, 4, 1, fin);
                                unsigned int modelNumLayerOptions = 0;
                                fread(&modelNumLayerOptions, 4, 1, fin);
                                unsigned int modelLayerOptionsOffset = 0;
                                fread(&modelLayerOptionsOffset, 4, 1, fin);
                                unsigned int modelNumVariables = 0;
                                fread(&modelNumVariables, 4, 1, fin);
                                unsigned int modelVariablesOffset = 0;
                                fread(&modelVariablesOffset, 4, 1, fin);
                                int modelTransform = 0;
                                fread(&modelTransform, 4, 1, fin);
                                int modelDrawStatusAccessor = 0;
                                fread(&modelDrawStatusAccessor, 4, 1, fin);
                                unsigned int modelDrawStatus = 0;
                                fread(&modelDrawStatus, 4, 1, fin);
                                int modelColorAccessor = 0;
                                fread(&modelColorAccessor, 4, 1, fin);
                                unsigned int lightConfigOffset = 0;
                                if (LAYOUT == SceneLayout::FIFA2003)
                                    fread(&lightConfigOffset, 4, 1, fin);
                                unsigned int modelData = 0;
                                fread(&modelData, 4, 1, fin);
                                unsigned int refModel = 0;
                                fread(&refModel, 4, 1, fin);
                                auto mb_pos = ftell(fin);
                                string modelName;
                                if (modelNameOffset) {
                                    fseek(fin, modelNameOffset, SEEK_SET);
                                    modelName = ReadString(fin);
                                }
                                else
                                    modelName = "(null)";
                                if (modelDrawStatus != 0) {
                                    //Warning("mb_u2 != 0");
                                }
                                fprintf(fout, "                <ModelBlock NameFormat=\"%s\" RenderSlot=\"%d\" Order=\"%d\" LevelOfDetail=\"%d\">\n", modelName.c_str(), mb_u1, mb_u2, mb_u3);
                                if (modelNumArguments > 0) {
                                    fputs("                    <FormatArguments>\n", fout);
                                    fseek(fin, modelArgumentsOffset, SEEK_SET);
                                    for (unsigned int a = 0; a < modelNumArguments; a++) {
                                        int modelFormatArgumentAccessor = 0;
                                        fread(&modelFormatArgumentAccessor, 4, 1, fin);
                                        fread(&value, 4, 1, fin);
                                        fprintf(fout, "                        <FormatArgumentAccessor>%s</FormatArgumentAccessor>%s\n", ACCESSOR_PTR(modelFormatArgumentAccessor).c_str(), ACCESSOR_COMMENT(modelFormatArgumentAccessor).c_str());
                                    }
                                    fputs("                    </FormatArguments>\n", fout);
                                }
                                if (nearClipAccessor != -1)
                                    fprintf(fout, "                    <NearClipAccessor>%s</NearClipAccessor>%s\n", ACCESSOR_PTR(nearClipAccessor).c_str(), ACCESSOR_COMMENT(nearClipAccessor).c_str());
                                if (farClipAccessor != -1)
                                    fprintf(fout, "                    <FarClipAccessor>%s</FarClipAccessor>%s\n", ACCESSOR_PTR(farClipAccessor).c_str(), ACCESSOR_COMMENT(farClipAccessor).c_str());
                                if (LAYOUT == SceneLayout::FIFA06 || LAYOUT == SceneLayout::CL0405 || LAYOUT == SceneLayout::FIFA2005) {
                                    if (fifaId != -1)
                                        fprintf(fout, "                    <MorphWeightsAccessor>%s</MorphWeightsAccessor>%s\n", ACCESSOR_PTR(fifaId).c_str(), ACCESSOR_COMMENT(fifaId).c_str());
                                    if (morphInfo != -1)
                                        fprintf(fout, "                    <MorphWeightsCountAccessor>%s</MorphWeightsCountAccessor>%s\n", ACCESSOR_PTR(morphInfo).c_str(), ACCESSOR_COMMENT(morphInfo).c_str());
                                }
                                if (modelTransform != -1)
                                    fprintf(fout, "                    <ModelTransformAccessor>%s</ModelTransformAccessor>%s\n", ACCESSOR_PTR(modelTransform).c_str(), ACCESSOR_COMMENT(modelTransform).c_str());
                                if (modelDrawStatusAccessor != -1)
                                    fprintf(fout, "                    <ModelDrawStatusAccessor>%s</ModelDrawStatusAccessor>%s\n", ACCESSOR_PTR(modelDrawStatusAccessor).c_str(), ACCESSOR_COMMENT(modelDrawStatusAccessor).c_str());
                                if (modelColorAccessor != -1)
                                    fprintf(fout, "                    <ModelColorAccessor>%s</ModelColorAccessor>%s\n", ACCESSOR_PTR(modelColorAccessor).c_str(), ACCESSOR_COMMENT(modelColorAccessor).c_str());
                                if (modelNumLayerOptions > 0) {
                                    fputs("                    <LayerOptions>\n", fout);
                                    fseek(fin, modelLayerOptionsOffset, SEEK_SET);
                                    for (unsigned int o = 0; o < modelNumLayerOptions; o++) {
                                        unsigned int layerNameIndex = 0;
                                        fread(&layerNameIndex, 4, 1, fin);
                                        int layerStateAccessor = 0;
                                        fread(&layerStateAccessor, 4, 1, fin);
                                        unsigned int layerDefaultState = 0;
                                        fread(&layerDefaultState, 4, 1, fin);
                                        string layerName;
                                        if (LAYOUT == SceneLayout::FIFA06)
                                            layerName = layerNames[layerNameIndex];
                                        else {
                                            auto layerOptionPos = ftell(fin);
                                            fseek(fin, layerNameIndex, SEEK_SET);
                                            layerName = ReadString(fin);
                                            fseek(fin, layerOptionPos, SEEK_SET);
                                        }
                                        fprintf(fout, "                        <LayerOption LayerName=\"%s\">\n", layerName.c_str());
                                        fprintf(fout, "                            <LayerStateAccessor>%s</LayerStateAccessor>%s\n", ACCESSOR_PTR(layerStateAccessor).c_str(), ACCESSOR_COMMENT(layerStateAccessor).c_str());
                                        fputs("                        </LayerOption>\n", fout);
                                    }
                                    fputs("                    </LayerOptions>\n", fout);
                                }
                                if (modelNumVariables > 0) {
                                    fputs("                    <Variables>\n", fout);
                                    fseek(fin, modelVariablesOffset, SEEK_SET);
                                    for (unsigned int o = 0; o < modelNumVariables; o++) {
                                        unsigned int varNameIndex = 0;
                                        fread(&varNameIndex, 4, 1, fin);
                                        int variableValueAccessor = 0;
                                        fread(&variableValueAccessor, 4, 1, fin);
                                        string variableName;
                                        if (LAYOUT == SceneLayout::FIFA06)
                                            variableName = variableNames[varNameIndex];
                                        else {
                                            auto varPos = ftell(fin);
                                            fseek(fin, varNameIndex, SEEK_SET);
                                            variableName = ReadString(fin);
                                            fseek(fin, varPos, SEEK_SET);
                                        }
                                        fprintf(fout, "                        <Variable Name=\"%s\">\n", variableName.c_str());
                                        fprintf(fout, "                            <ValueAccessor>%s</ValueAccessor>%s\n", ACCESSOR_PTR(variableValueAccessor).c_str(), ACCESSOR_COMMENT(variableValueAccessor).c_str());
                                        fputs("                        </Variable>\n", fout);
                                    }
                                    fputs("                    </Variables>\n", fout);
                                }
                                if (lightConfigOffset > 0) {
                                    fputs("                    <LightConfig>\n", fout);
                                    fseek(fin, lightConfigOffset, SEEK_SET);
                                    unsigned int lightSet1Accessor = 0;
                                    fread(&lightSet1Accessor, 4, 1, fin);
                                    unsigned int lightSet1Value = 0;
                                    fread(&lightSet1Value, 4, 1, fin);
                                    unsigned int lightSet2Accessor = 0;
                                    fread(&lightSet2Accessor, 4, 1, fin);
                                    unsigned int lightSet2Value = 0;
                                    fread(&lightSet2Value, 4, 1, fin);
                                    unsigned int lightSetInterpolateAccessor = 0;
                                    fread(&lightSetInterpolateAccessor, 4, 1, fin);
                                    unsigned int lightSetInterpolateValue = 0;
                                    fread(&lightSetInterpolateValue, 4, 1, fin);
                                    fprintf(fout, "                        <Set1Accessor>%s</Set1Accessor>%s\n", ACCESSOR_PTR(lightSet1Accessor).c_str(), ACCESSOR_COMMENT(lightSet1Accessor).c_str());
                                    fprintf(fout, "                        <Set2Accessor>%s</Set2Accessor>%s\n", ACCESSOR_PTR(lightSet2Accessor).c_str(), ACCESSOR_COMMENT(lightSet2Accessor).c_str());
                                    fprintf(fout, "                        <InterpolateAccessor>%s</InterpolateAccessor>%s\n", ACCESSOR_PTR(lightSetInterpolateAccessor).c_str(), ACCESSOR_COMMENT(lightSetInterpolateAccessor).c_str());
                                    fputs("                    </LightConfig>\n", fout);
                                }
                                fputs("                </ModelBlock>\n", fout);
                                fseek(fin, mb_pos, SEEK_SET);
                            }
                            fputs("            </ModelBlocks>\n", fout);
                        }
                        fputs("        </SceneEntry>\n", fout);
                        fseek(fin, pos, SEEK_SET);
                    }
                    fputs("    </SceneEntries>\n", fout);
                }
                if (numLoaders > 0) {
                    fseek(fin, loadersOffset, SEEK_SET);
                    fputs("    <SceneCalls>\n", fout);
                    for (unsigned int i = 0; i < numLoaders; i++) {
                        unsigned int type = 0;
                        fread(&type, 4, 1, fin);
                        if (type == 0) {
                            fread(&value, 4, 1, fin);
                            fprintf(fout, "        <SceneCall TargetEntry=\"%s\"></SceneCall>\n", sceneEntryName[value].c_str());
                        }
                        else {
                            fprintf(fout, "        <SceneCall>\n");
                            int sceneCallbackAccessor = 0;
                            fread(&sceneCallbackAccessor, 4, 1, fin);
                            fprintf(fout, "            <CallbackAccessor>%s</CallbackAccessor>%s\n", ACCESSOR_PTR(sceneCallbackAccessor).c_str(), ACCESSOR_COMMENT(sceneCallbackAccessor).c_str());
                            fputs("        </SceneCall>\n", fout);
                        }
                    }
                    fputs("    </SceneCalls>\n", fout);
                }
                if (numParameters > 0) {
                    fseek(fin, parametersOffset, SEEK_SET);
                    fputs("    <DebugEntries>\n", fout);
                    for (unsigned int i = 0; i < numParameters; i++) {
                        unsigned int debugVarNameOffset = 0;
                        fread(&debugVarNameOffset, 4, 1, fin);
                        unsigned int debugLayerNameOffset = 0;
                        fread(&debugLayerNameOffset, 4, 1, fin);
                        unsigned int debugVarTypeOffset = 0;
                        fread(&debugVarTypeOffset, 4, 1, fin);
                        int unkDebugAccessor = 0;
                        fread(&unkDebugAccessor, 4, 1, fin);
                        int unkDebug1 = 0;
                        fread(&unkDebug1, 4, 1, fin);
                        auto debugEntryPos = ftell(fin);
                        fseek(fin, debugVarNameOffset, SEEK_SET);
                        auto debugVarName = ReadString(fin);
                        fseek(fin, debugLayerNameOffset, SEEK_SET);
                        auto debugLayerName = ReadString(fin);
                        fseek(fin, debugVarTypeOffset, SEEK_SET);
                        auto debugVarType = ReadString(fin);
                        if (debugVarType.starts_with("list")) {
                            string tmpType = "list { ";
                            bool tmpFirst = true;
                            for (unsigned int t = 5; t < debugVarType.size(); t++) {
                                if (debugVarType[t] == 0xA)
                                    tmpType += ",";
                                else
                                    tmpType += debugVarType[t];
                            }
                            tmpType += " }";
                            debugVarType = tmpType;
                        }
                        fseek(fin, debugEntryPos, SEEK_SET);
                        fprintf(fout, "        <DebugEntry VarName=\"%s\" LayerName=\"%s\" VarType=\"%s\">\n", debugVarName.c_str(), debugLayerName.c_str(), debugVarType.c_str());
                        fprintf(fout, "            <ValueAccessor>%s</ValueAccessor>%s\n", ACCESSOR_PTR(unkDebugAccessor).c_str(), ACCESSOR_COMMENT(unkDebugAccessor).c_str());
                        fputs("        </DebugEntry>\n", fout);
                    }
                    fputs("    </DebugEntries>\n", fout);
                }
                if (numUnkEntries2 > 0) {
                    fseek(fin, unkEntries2Offset, SEEK_SET);
                    fputs("    <UnknownEntriesCL0405>\n", fout);
                    for (unsigned int i = 0; i < numUnkEntries2; i++) {
                        int unkcl0405accessor = 0;
                        fread(&unkcl0405accessor, 4, 1, fin);
                        fprintf(fout, "        <UnknownAccessor>%s</UnknownAccessor>%s\n", ACCESSOR_PTR(unkcl0405accessor).c_str(), ACCESSOR_COMMENT(unkcl0405accessor).c_str());
                    }
                    fputs("    </UnknownEntriesCL0405>\n", fout);
                }
            }
            fputs("</SceneFile>\n", fout);
            fclose(fout);
        }
        fclose(fin);
    }
}

void compile(path const &inPath, path const &outPath) {
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(inPath.c_str());
    if (result) {
        // collect data from xml
        using accessor = string;

        struct Texture {
            string nameFormat;
            vector<accessor> args;

            struct Offsets {
                unsigned int nameFormat = 0;
                unsigned int args = 0;
            } offsets;
        };

        struct TextureAssignment {
            string targetTag;
            accessor sourceTag;
        };

        struct TexBlock {
            vector<Texture> textures;
            vector<TextureAssignment> textureAssignments;
            string name;

            struct Offsets {
                unsigned int textures = 0;
                unsigned int textureAssignments = 0;
                unsigned int name = 0;
            } offsets;
        };

        struct LayerOption {
            string name;
            accessor state;

            struct Offsets {
                unsigned int name = 0;
            } offsets;
        };

        struct Variable {
            string name;
            accessor value;

            struct Offsets {
                unsigned int name = 0;
            } offsets;
        };

        struct LightConfig {
            accessor lightSet1;
            accessor lightSet2;
            accessor lightSetInterpolate;
        };

        struct ModelBlock {
            unsigned int u1 = 0;
            string nameFormat;
            accessor nearClip;
            accessor farClip;
            unsigned int u2 = 0;
            unsigned int u3 = 0;
            accessor morphWeights;
            accessor morphWeightsCount;
            vector<accessor> args;
            vector<LayerOption> layerOptions;
            vector<Variable> variables;
            accessor transform;
            accessor drawStatus;
            accessor color;
            bool hasLightConfig = false;
            LightConfig lightConfig;

            struct Offsets {
                unsigned int nameFormat;
                unsigned int args;
                unsigned int layerOptions;
                unsigned int variables;
                unsigned int lightConfig;
            } offsets;
        };

        struct SceneEntry {
            accessor objectIdAccessor;
            unsigned int objectId = 0;
            accessor drawStatus;
            accessor pose;
            accessor transform;
            bool boundingUsed = false;
            float boundingCenter[3] = { 0.0f, 0.0f, 0.0f };
            float boundingRadius = 0.0f;
            accessor ps2kvalue;
            vector<TexBlock> texBlocks;
            vector<ModelBlock> modelBlocks;
            accessor init;
            accessor render;
            unsigned int renderSlot = 0;
            accessor purge;
            accessor object;
            string poolName;
            string name;

            struct Offsets {
                unsigned int texBlocks = 0;
                unsigned int modelBlocks = 0;
                unsigned int poolName = 0;
                unsigned int name = 0;
            } offsets;
        };

        struct SceneCall {
            string entry;
            accessor callback;
        };

        struct DebugEntry {
            string varName;
            string layerName;
            string varType;
            accessor value;

            struct Offsets {
                unsigned int varName = 0;
                unsigned int layerName = 0;
                unsigned int varType = 0;
            } offsets;
        };

        vector<SceneEntry> SceneEntries;
        vector<SceneCall> SceneCalls;
        vector<DebugEntry> DebugEntries;
        vector<accessor> CL0405Callbacks;
        map<accessor, int> Accessors;
        map<string, unsigned int> LayerNames;
        map<string, unsigned int> VariableNames;
        unsigned int numRenderSlots = 0;

        auto sceneRootNode = doc.child("SceneFile");
        if (sceneRootNode) {
            auto layoutNode = sceneRootNode.child("Layout");
            if (layoutNode) {
                string layout = ToLower(layoutNode.attribute("Type").as_string());
                if (layout == "fifa2003")
                    LAYOUT = SceneLayout::FIFA2003;
                else if (layout == "fifa2004")
                    LAYOUT = SceneLayout::FIFA2004;
                else if (layout == "fifa2005")
                    LAYOUT = SceneLayout::FIFA2005;
                else if (layout == "cl0405")
                    LAYOUT = SceneLayout::CL0405;
                else if (layout == "fifa06")
                    LAYOUT = SceneLayout::FIFA06;
                numRenderSlots = layoutNode.attribute("NumRenderSlots").as_uint();
            }
            if (LAYOUT != SceneLayout::Unknown) {
                auto sceneEntriesNode = sceneRootNode.child("SceneEntries");
                if (sceneEntriesNode) {
                    for (auto sceneEntryNode = sceneEntriesNode.child("SceneEntry"); sceneEntryNode; sceneEntryNode = sceneEntryNode.next_sibling("SceneEntry")) {
                        SceneEntry sceneEntry;
                        sceneEntry.objectIdAccessor = sceneEntryNode.child_value("ObjectIdAccessor");
                        sceneEntry.objectId = sceneEntryNode.attribute("ObjectId").as_uint();
                        sceneEntry.drawStatus = sceneEntryNode.child_value("DrawStatusAccessor");
                        sceneEntry.pose = sceneEntryNode.child_value("PoseAccessor");
                        sceneEntry.transform = sceneEntryNode.child_value("TransformAccessor");
                        auto boundCenterAttribute = sceneEntryNode.attribute("BoundingCenter");
                        auto boundingRadiusAttribute = sceneEntryNode.attribute("BoundingRadius");
                        bool hasBoundingCenter = false;
                        if (!boundCenterAttribute.empty()) {
                            string strBoundCenter = boundCenterAttribute.as_string();
                            auto boundCenterParts = Split(strBoundCenter, ',');
                            if (boundCenterParts.size() == 3) {
                                sceneEntry.boundingCenter[0] = SafeConvertFloat(boundCenterParts[0]);
                                sceneEntry.boundingCenter[1] = SafeConvertFloat(boundCenterParts[1]);
                                sceneEntry.boundingCenter[2] = SafeConvertFloat(boundCenterParts[2]);
                                hasBoundingCenter = true;
                            }
                        }
                        sceneEntry.boundingRadius = boundingRadiusAttribute.as_float();
                        sceneEntry.boundingUsed = hasBoundingCenter || !boundingRadiusAttribute.empty();
                        sceneEntry.ps2kvalue = sceneEntryNode.child_value("KValueAccessor");
                        sceneEntry.init = sceneEntryNode.child_value("InitAccessor");
                        sceneEntry.render = sceneEntryNode.child_value("PreRenderAccessor");
                        sceneEntry.renderSlot = sceneEntryNode.attribute("RenderSlot").as_uint();
                        sceneEntry.purge = sceneEntryNode.child_value("PurgeAccessor");
                        sceneEntry.object = sceneEntryNode.child_value("ObjectDataAccessor");
                        if (LAYOUT != SceneLayout::FIFA2003 && LAYOUT != SceneLayout::FIFA2004)
                            sceneEntry.poolName = sceneEntryNode.attribute("PoolName").as_string();
                        sceneEntry.name = sceneEntryNode.attribute("Name").as_string();
                        auto texBlocksNode = sceneEntryNode.child("TexBlocks");
                        if (texBlocksNode) {
                            for (auto texBlockNode = texBlocksNode.child("TexBlock"); texBlockNode; texBlockNode = texBlockNode.next_sibling("TexBlock")) {
                                TexBlock texBlock;
                                texBlock.name = texBlockNode.attribute("Name").as_string();
                                auto texturesNode = texBlockNode.child("Textures");
                                if (texturesNode) {
                                    for (auto textureNode = texturesNode.child("Texture"); textureNode; textureNode = textureNode.next_sibling("Texture")) {
                                        Texture texture;
                                        texture.nameFormat = textureNode.attribute("NameFormat").as_string();
                                        auto formatArgumentsNode = textureNode.child("FormatArguments");
                                        if (formatArgumentsNode) {
                                            for (auto formatArgumentAccessorNode = formatArgumentsNode.child("FormatArgumentAccessor"); formatArgumentAccessorNode; formatArgumentAccessorNode = formatArgumentAccessorNode.next_sibling("FormatArgumentAccessor"))
                                                texture.args.push_back(formatArgumentAccessorNode.child_value());
                                        }
                                        texBlock.textures.push_back(texture);
                                    }
                                }
                                auto textureAssignmentsNode = texBlockNode.child("TextureAssignments");
                                if (textureAssignmentsNode) {
                                    for (auto textureAssignmentNode = textureAssignmentsNode.child("TextureAssignment"); textureAssignmentNode; textureAssignmentNode = textureAssignmentNode.next_sibling("TextureAssignment")) {
                                        TextureAssignment textureAssignment;
                                        textureAssignment.targetTag = textureAssignmentNode.attribute("TargetTag").as_string();
                                        textureAssignment.sourceTag = textureAssignmentNode.child_value("SourceTagAccessor");
                                        texBlock.textureAssignments.push_back(textureAssignment);
                                    }
                                }
                                sceneEntry.texBlocks.push_back(texBlock);
                            }
                        }

                        auto modelBlocksNode = sceneEntryNode.child("ModelBlocks");
                        if (modelBlocksNode) {
                            for (auto modelBlockNode = modelBlocksNode.child("ModelBlock"); modelBlockNode; modelBlockNode = modelBlockNode.next_sibling("ModelBlock")) {
                                ModelBlock modelBlock;
                                modelBlock.nameFormat = modelBlockNode.attribute("NameFormat").as_string();
                                modelBlock.u1 = modelBlockNode.attribute("RenderSlot").as_uint();
                                modelBlock.u2 = modelBlockNode.attribute("Order").as_uint();
                                modelBlock.u3 = modelBlockNode.attribute("LevelOfDetail").as_uint();
                                modelBlock.nearClip = modelBlockNode.child_value("NearClipAccessor");
                                modelBlock.farClip = modelBlockNode.child_value("FarClipAccessor");
                                if (LAYOUT == SceneLayout::FIFA06 || LAYOUT == SceneLayout::CL0405 || LAYOUT == SceneLayout::FIFA2005) {
                                    modelBlock.morphWeights = modelBlockNode.child_value("MorphWeightsAccessor");
                                    modelBlock.morphWeightsCount = modelBlockNode.child_value("MorphWeightsCountAccessor");
                                }
                                modelBlock.transform = modelBlockNode.child_value("ModelTransformAccessor");
                                modelBlock.drawStatus = modelBlockNode.child_value("ModelDrawStatusAccessor");
                                modelBlock.color = modelBlockNode.child_value("ModelColorAccessor");
                                auto formatArgumentsNode = modelBlockNode.child("FormatArguments");
                                if (formatArgumentsNode) {
                                    for (auto formatArgumentAccessorNode = formatArgumentsNode.child("FormatArgumentAccessor"); formatArgumentAccessorNode; formatArgumentAccessorNode = formatArgumentAccessorNode.next_sibling("FormatArgumentAccessor"))
                                        modelBlock.args.push_back(formatArgumentAccessorNode.child_value());
                                }
                                auto layerOptionsNode = modelBlockNode.child("LayerOptions");
                                if (layerOptionsNode) {
                                    for (auto layerOptionNode = layerOptionsNode.child("LayerOption"); layerOptionNode; layerOptionNode = layerOptionNode.next_sibling("LayerOption")) {
                                        LayerOption layerOption;
                                        layerOption.name = layerOptionNode.attribute("LayerName").as_string();
                                        layerOption.state = layerOptionNode.child_value("LayerStateAccessor");
                                        modelBlock.layerOptions.push_back(layerOption);
                                    }
                                }
                                auto variablesNode = modelBlockNode.child("Variables");
                                if (variablesNode) {
                                    for (auto variableNode = variablesNode.child("Variable"); variableNode; variableNode = variableNode.next_sibling("Variable")) {
                                        Variable variable;
                                        variable.name = variableNode.attribute("Name").as_string();
                                        variable.value = variableNode.child_value("ValueAccessor");
                                        modelBlock.variables.push_back(variable);
                                    }
                                }
                                if (LAYOUT == SceneLayout::FIFA2003) {
                                    auto lightConfigNode = modelBlockNode.child("LightConfig");
                                    if (lightConfigNode) {
                                        modelBlock.lightConfig.lightSet1 = lightConfigNode.child_value("Set1Accessor");
                                        modelBlock.lightConfig.lightSet2 = lightConfigNode.child_value("Set2Accessor");
                                        modelBlock.lightConfig.lightSetInterpolate = lightConfigNode.child_value("InterpolateAccessor");
                                        modelBlock.hasLightConfig = true;
                                    }
                                }
                                sceneEntry.modelBlocks.push_back(modelBlock);
                            }
                        }
                        SceneEntries.push_back(sceneEntry);
                    }
                }
                auto sceneCallsNode = sceneRootNode.child("SceneCalls");
                if (sceneCallsNode) {
                    for (auto sceneCallNode = sceneCallsNode.child("SceneCall"); sceneCallNode; sceneCallNode = sceneCallNode.next_sibling("SceneCall")) {
                        SceneCall sceneCall;
                        auto targetEntryAttribute = sceneCallNode.attribute("TargetEntry");
                        if (!targetEntryAttribute.empty())
                            sceneCall.entry = targetEntryAttribute.as_string();
                        else
                            sceneCall.callback = sceneCallNode.child_value("CallbackAccessor");
                        SceneCalls.push_back(sceneCall);
                    }
                }
                auto unknownEntriesCL0405Node = sceneRootNode.child("UnknownEntriesCL0405");
                if (unknownEntriesCL0405Node) {
                    for (auto unknownAccessorNode = unknownEntriesCL0405Node.child("UnknownAccessor"); unknownAccessorNode; unknownAccessorNode = unknownAccessorNode.next_sibling("UnknownAccessor"))
                        CL0405Callbacks.push_back(unknownAccessorNode.child_value());
                }
                auto debugEntriesNode = sceneRootNode.child("DebugEntries");
                if (debugEntriesNode) {
                    for (auto debugEntryNode = debugEntriesNode.child("DebugEntry"); debugEntryNode; debugEntryNode = debugEntryNode.next_sibling("DebugEntry")) {
                        DebugEntry debugEntry;
                        debugEntry.varName = debugEntryNode.attribute("VarName").as_string();
                        debugEntry.layerName = debugEntryNode.attribute("LayerName").as_string();
                        debugEntry.varType = debugEntryNode.attribute("VarType").as_string();
                        debugEntry.value = debugEntryNode.child_value("ValueAccessor");
                        DebugEntries.push_back(debugEntry);
                    }
                }
            }
        }

        //

        // write data to .cs file

        BinaryBuffer out;
        out.Put('MSGS');
        if (LAYOUT == SceneLayout::FIFA2003) {
            unsigned int header[] = { 11, 100, 104, 20, 8, 8, 24, 12, 20, 84, 12, 8, 24 };
            out.Put(header, std::size(header) * 4);
        }
        else if (LAYOUT == SceneLayout::FIFA2004) {
            unsigned int header[] = { 11, 96, 104, 20, 8, 8, 24, 12, 20, 80, 12, 8 };
            out.Put(header, std::size(header) * 4);
        }
        else if (LAYOUT == SceneLayout::FIFA2005) {
            unsigned int header[] = { 11, 96, 108, 20, 8, 8, 24, 12, 20, 88, 12, 8 };
            out.Put(header, std::size(header) * 4);
        }
        else if (LAYOUT == SceneLayout::CL0405) {
            unsigned int header[] = { 11, 112, 108, 20, 8, 4, 8, 24, 12, 20, 88, 12, 8, 0xFFFFFFFF };
            out.Put(header, std::size(header) * 4);
        }
        else if (LAYOUT == SceneLayout::FIFA06) {
            unsigned int header[] = { 13, 112, 108, 20, 8, 8, 24, 12, 20, 88, 4, 12, 4, 8 };
            out.Put(header, std::size(header) * 4);
        }
        out.Put<int>(0);

        auto PutAccessor = [&](BinaryBuffer &binBuf, accessor a) {
            int index = -1;
            if (!a.empty()) {
                if (LAYOUT == SceneLayout::FIFA06) {
                    try {
                        index = static_cast<int>(std::stoull(a));
                    }
                    catch (...) {}
                }
                else {
                    auto it = Accessors.find(a);
                    if (it != Accessors.end())
                        index = (*it).second;
                    else {
                        index = Accessors.size();
                        Accessors[a] = index;
                    }
                }
            }
            binBuf.Put(index);
        };

        auto PutOffset = [](BinaryBuffer &binBuf, unsigned int &outOffset) {
            outOffset = binBuf.Position();
            binBuf.Put<int>(0);
        };

        auto PutAt = [](BinaryBuffer &binBuf, unsigned int pos, auto value) {
            auto savedPos = binBuf.Position();
            binBuf.MoveTo(pos);
            binBuf.Put(value);
            binBuf.MoveTo(savedPos);
        };

        unsigned int sceneEntriesOffset = 0;
        unsigned int debugEntriesOffset = 0;
        unsigned int sceneCallsOffset = 0;
        unsigned int layerNamesCountOffset = 0;
        unsigned int layerNamesOffset = 0;
        unsigned int variableNamesCountOffset = 0;
        unsigned int variableNamesOffset = 0;
        unsigned int stringsCountOffset = 0;
        unsigned int stringsOffset = 0;
        unsigned int cl0405EntriesOffset = 0;

        if (LAYOUT != SceneLayout::FIFA06) {
            PutOffset(out, stringsCountOffset);
            PutOffset(out, stringsOffset);
        }
        out.Put(SceneEntries.size());
        PutOffset(out, sceneEntriesOffset);
        out.Put(numRenderSlots);
        out.Put(DebugEntries.size());
        PutOffset(out, debugEntriesOffset);
        out.Put(SceneCalls.size());
        PutOffset(out, sceneCallsOffset);
        if (LAYOUT == SceneLayout::FIFA06) {
            PutOffset(out, layerNamesCountOffset);
            PutOffset(out, layerNamesOffset);
            PutOffset(out, variableNamesCountOffset);
            PutOffset(out, variableNamesOffset);
        }
        if (LAYOUT == SceneLayout::CL0405) {
            out.Put(CL0405Callbacks.size());
            PutOffset(out, cl0405EntriesOffset);
        }
        out.Put<unsigned int>(0xFEDCBA98);

        PutAt(out, sceneEntriesOffset, out.Position());

        for (auto &sceneEntry : SceneEntries) {
            PutAccessor(out, sceneEntry.objectIdAccessor);
            out.Put(sceneEntry.objectId);
            out.Put<int>(0);
            PutAccessor(out, sceneEntry.drawStatus);
            out.Put<int>(0);
            PutAccessor(out, sceneEntry.pose);
            PutAccessor(out, sceneEntry.transform);
            out.Put<unsigned int>(sceneEntry.boundingUsed);
            out.Put(sceneEntry.boundingCenter[0]);
            out.Put(sceneEntry.boundingCenter[1]);
            out.Put(sceneEntry.boundingCenter[2]);
            out.Put(sceneEntry.boundingRadius);
            PutAccessor(out, sceneEntry.ps2kvalue);
            out.Put<int>(0);
            out.Put(sceneEntry.texBlocks.size());
            PutOffset(out, sceneEntry.offsets.texBlocks);
            out.Put(sceneEntry.modelBlocks.size());
            PutOffset(out, sceneEntry.offsets.modelBlocks);
            PutAccessor(out, sceneEntry.init);
            PutAccessor(out, sceneEntry.render);
            out.Put(sceneEntry.renderSlot);
            PutAccessor(out, sceneEntry.purge);
            PutAccessor(out, sceneEntry.object);
            out.Put<int>(0);
            if (LAYOUT != SceneLayout::FIFA2003 && LAYOUT != SceneLayout::FIFA2004)
                PutOffset(out, sceneEntry.offsets.poolName);
            PutOffset(out, sceneEntry.offsets.name);
            out.Put<unsigned int>(305419896);
        }

        for (auto &sceneEntry : SceneEntries) {
            if (LAYOUT != SceneLayout::FIFA2003 && LAYOUT != SceneLayout::FIFA2004) {
                PutAt(out, sceneEntry.offsets.poolName, out.Position());
                out.Put(sceneEntry.poolName);
            }
            PutAt(out, sceneEntry.offsets.name, out.Position());
            out.Put(sceneEntry.name);
            out.Align(4);
            PutAt(out, sceneEntry.offsets.texBlocks, out.Position());
            for (auto &texBlock : sceneEntry.texBlocks) {
                out.Put(texBlock.textures.size());
                PutOffset(out, texBlock.offsets.textures);
                out.Put(texBlock.textureAssignments.size());
                PutOffset(out, texBlock.offsets.textureAssignments);
                PutOffset(out, texBlock.offsets.name);
                out.Put<int>(0);
            }
            PutAt(out, sceneEntry.offsets.modelBlocks, out.Position());
            for (auto &modelBlock : sceneEntry.modelBlocks) {
                out.Put(modelBlock.u1);
                PutOffset(out, modelBlock.offsets.nameFormat);
                PutAccessor(out, modelBlock.nearClip);
                out.Put(0.0f);
                PutAccessor(out, modelBlock.farClip);
                out.Put(0.0f);
                out.Put(modelBlock.u2);
                out.Put(modelBlock.u3);
                if (LAYOUT == SceneLayout::FIFA06 || LAYOUT == SceneLayout::CL0405 || LAYOUT == SceneLayout::FIFA2005) {
                    PutAccessor(out, modelBlock.morphWeights);
                    PutAccessor(out, modelBlock.morphWeightsCount);
                }
                out.Put(modelBlock.args.size());
                PutOffset(out, modelBlock.offsets.args);
                out.Put(modelBlock.layerOptions.size());
                PutOffset(out, modelBlock.offsets.layerOptions);
                out.Put(modelBlock.variables.size());
                PutOffset(out, modelBlock.offsets.variables);
                PutAccessor(out, modelBlock.transform);
                PutAccessor(out, modelBlock.drawStatus);
                out.Put<int>(0);
                PutAccessor(out, modelBlock.color);
                if (LAYOUT == SceneLayout::FIFA2003)
                    PutOffset(out, modelBlock.offsets.lightConfig);
                out.Put<int>(0);
                out.Put<int>(0);
            }
        }

        for (auto &sceneEntry : SceneEntries) {
            for (auto &texBlock : sceneEntry.texBlocks) {
                PutAt(out, texBlock.offsets.name, out.Position());
                out.Put(texBlock.name);
                out.Align(4);
            }
            for (auto &texBlock : sceneEntry.texBlocks) {
                PutAt(out, texBlock.offsets.textures, out.Position());
                for (auto &texture : texBlock.textures) {
                    PutOffset(out, texture.offsets.nameFormat);
                    out.Put(texture.args.size());
                    PutOffset(out, texture.offsets.args);
                }
            }
            for (auto &texBlock : sceneEntry.texBlocks) {
                PutAt(out, texBlock.offsets.textureAssignments, out.Position());
                for (auto &textureAssignment : texBlock.textureAssignments) {
                    char targetTag[4] = {};
                    strncpy(targetTag, textureAssignment.targetTag.c_str(), 4);
                    reverse(&targetTag[0], &targetTag[4]);
                    out.Put(targetTag, 4);
                    PutAccessor(out, textureAssignment.sourceTag);
                    out.Put<int>(0);
                    out.Put<int>(0);
                    out.Put<int>(0);
                }
            }
            for (auto &modelBlock : sceneEntry.modelBlocks) {
                if (ToLower(modelBlock.nameFormat) != "(null)") {
                    PutAt(out, modelBlock.offsets.nameFormat, out.Position());
                    out.Put(modelBlock.nameFormat);
                    out.Align(4);
                }
                if (!modelBlock.args.empty()) {
                    PutAt(out, modelBlock.offsets.args, out.Position());
                    for (auto &arg : modelBlock.args) {
                        PutAccessor(out, arg);
                        out.Put<int>(0);
                    }
                }
                if (!modelBlock.layerOptions.empty()) {
                    PutAt(out, modelBlock.offsets.layerOptions, out.Position());
                    for (auto &layerOption : modelBlock.layerOptions) {
                        if (LAYOUT == SceneLayout::FIFA06) {
                            unsigned int index = 0;
                            auto it = LayerNames.find(layerOption.name);
                            if (it != LayerNames.end())
                                index = (*it).second;
                            else {
                                index = LayerNames.size();
                                LayerNames[layerOption.name] = index;
                            }
                            out.Put(index);
                        }
                        else
                            PutOffset(out, layerOption.offsets.name);
                        PutAccessor(out, layerOption.state);
                        out.Put<int>(0);
                    }
                }
                if (!modelBlock.variables.empty()) {
                    PutAt(out, modelBlock.offsets.variables, out.Position());
                    for (auto &variable : modelBlock.variables) {
                        if (LAYOUT == SceneLayout::FIFA06) {
                            unsigned int index = 0;
                            auto it = VariableNames.find(variable.name);
                            if (it != VariableNames.end())
                                index = (*it).second;
                            else {
                                index = VariableNames.size();
                                VariableNames[variable.name] = index;
                            }
                            out.Put(index);
                        }
                        else
                            PutOffset(out, variable.offsets.name);
                        PutAccessor(out, variable.value);
                    }
                }
                if (LAYOUT == SceneLayout::FIFA2003) {
                    if (modelBlock.hasLightConfig) {
                        PutAt(out, modelBlock.offsets.lightConfig, out.Position());
                        PutAccessor(out, modelBlock.lightConfig.lightSet1);
                        out.Put<int>(0);
                        PutAccessor(out, modelBlock.lightConfig.lightSet2);
                        out.Put<int>(0);
                        PutAccessor(out, modelBlock.lightConfig.lightSetInterpolate);
                        out.Put<int>(0);
                    }
                }
            }
        }

        for (auto &sceneEntry : SceneEntries) {
            for (auto &texBlock : sceneEntry.texBlocks) {
                for (auto &texture : texBlock.textures) {
                    if (ToLower(texture.nameFormat) != "(null)") {
                        PutAt(out, texture.offsets.nameFormat, out.Position());
                        out.Put(texture.nameFormat);
                        out.Align(4);
                    }
                    PutAt(out, texture.offsets.args, out.Position());
                    for (auto &arg : texture.args) {
                        PutAccessor(out, arg);
                        out.Put<int>(0);
                    }
                }
            }
            if (LAYOUT != SceneLayout::FIFA06) {
                for (auto &modelBlock : sceneEntry.modelBlocks) {
                    for (auto &layerOption : modelBlock.layerOptions) {
                        PutAt(out, layerOption.offsets.name, out.Position());
                        out.Put(layerOption.name);
                        out.Align(4);
                    }
                    for (auto &variable : modelBlock.variables) {
                        PutAt(out, variable.offsets.name, out.Position());
                        out.Put(variable.name);
                        out.Align(4);
                    }
                }
            }
        }

        PutAt(out, debugEntriesOffset, out.Position());

        for (auto &debugEntry : DebugEntries) {
            PutOffset(out, debugEntry.offsets.varName);
            PutOffset(out, debugEntry.offsets.layerName);
            PutOffset(out, debugEntry.offsets.varType);
            PutAccessor(out, debugEntry.value);
            out.Put<unsigned int>(324508639);
        }
        for (auto &debugEntry : DebugEntries) {
            PutAt(out, debugEntry.offsets.varName, out.Position());
            out.Put(debugEntry.varName);
            out.Align(4);
            PutAt(out, debugEntry.offsets.layerName, out.Position());
            out.Put(debugEntry.layerName);
            out.Align(4);
            PutAt(out, debugEntry.offsets.varType, out.Position());
            if (debugEntry.varType.starts_with("list")) {
                string listVarType = "list";
                auto bp = debugEntry.varType.find('{');
                if (bp != string::npos) {
                    auto ep = debugEntry.varType.find('}', bp + 1);
                    string strValues = debugEntry.varType.substr(bp + 1, ep - bp - 1);
                    auto values = Split(strValues, ',', true, true, false);
                    for (auto const &v : values) {
                        try {
                            int intValue = static_cast<int>(std::stoull(v));
                            listVarType += "\n";
                            listVarType += Format("%d", intValue);
                        }
                        catch (...) {}
                    }
                }
                out.Put(listVarType);
            }
            else
                out.Put(debugEntry.varType);
            out.Align(4);
        }

        PutAt(out, sceneCallsOffset, out.Position());

        for (auto &sceneCall : SceneCalls) {
            if (!sceneCall.entry.empty()) {
                unsigned int index = 0;
                for (unsigned int i = 0; i < SceneEntries.size(); i++) {
                    if (SceneEntries[i].name == sceneCall.entry) {
                        index = i;
                        break;
                    }
                }
                out.Put<unsigned int>(0);
                out.Put<unsigned int>(index);
            }
            else {
                out.Put<unsigned int>(1);
                PutAccessor(out, sceneCall.callback);
            }
        }

        if (LAYOUT == SceneLayout::FIFA06) {
            PutAt(out, layerNamesCountOffset, LayerNames.size());
            PutAt(out, layerNamesOffset, out.Position());
            unsigned int layerNamesPosition = out.Position();
            vector<string> vecLayerNames(LayerNames.size());
            for (auto &[layerName, index] : LayerNames) {
                vecLayerNames[index] = layerName;
                out.Put<unsigned int>(0);
            }
            PutAt(out, variableNamesCountOffset, VariableNames.size());
            PutAt(out, variableNamesOffset, out.Position());
            unsigned int variableNamesPosition = out.Position();
            vector<string> vecVariableNames(VariableNames.size());
            for (auto &[variableName, index] : VariableNames) {
                vecVariableNames[index] = variableName;
                out.Put<unsigned int>(0);
            }
            for (unsigned int i = 0; i < vecLayerNames.size(); i++) {
                out.Align(4);
                PutAt(out, layerNamesPosition + i * 4, out.Position());
                out.Put(vecLayerNames[i]);
            }
            for (unsigned int i = 0; i < vecVariableNames.size(); i++) {
                out.Align(4);
                PutAt(out, variableNamesPosition + i * 4, out.Position());
                out.Put(vecVariableNames[i]);
            }
        }

        if (LAYOUT == SceneLayout::CL0405) {
            PutAt(out, cl0405EntriesOffset, out.Position());
            for (auto cl0405Callback : CL0405Callbacks)
                PutAccessor(out, cl0405Callback);
        }

        if (LAYOUT != SceneLayout::FIFA06) {
            PutAt(out, stringsCountOffset, Accessors.size());
            PutAt(out, stringsOffset, out.Position());
            vector<string> vecAccessorNames(Accessors.size());
            for (auto &[accessorName, index] : Accessors)
                vecAccessorNames[index] = accessorName;
            for (auto accessorName : vecAccessorNames) {
                out.Put(accessorName);
                out.Align(4);
            }
        }

        out.WriteToFile(outPath);
    }
}

int main(int argc, char *argv[]) {
#ifdef COMPILER
    CommandLine cmd(argc, argv, { "i", "o" }, {});
    path in, out;
    if (cmd.HasArgument("i"))
        in = cmd.GetArgumentString("i");
    if (cmd.HasArgument("o"))
        out = cmd.GetArgumentString("o");
    if (in.empty())
        in = current_path();
    if (is_directory(in) != is_directory(out))
        out.clear();
    if (out.empty()) {
        out = in;
        if (!is_directory(in))
            out.replace_extension(".cs");
    }
    if (is_directory(in)) {
        for (auto const &i : directory_iterator(in)) {
            if (is_regular_file(i.path()) && cmd.ToLower(i.path().extension().string()) == ".xml")
                compile(i.path(), out / (i.path().stem().string() + ".cs"));
        }
    }
    else
        compile(in, out);
#endif
#ifdef DECOMPILER
    CommandLine cmd(argc, argv, { "i", "o", "baseAddress" }, {});
    path in, out;
    if (cmd.HasArgument("i"))
        in = cmd.GetArgumentString("i");
    if (cmd.HasArgument("o"))
        out = cmd.GetArgumentString("o");
    if (in.empty())
        in = current_path();
    if (is_directory(in) != is_directory(out))
        out.clear();
    if (out.empty()) {
        out = in;
        if (!is_directory(in))
            out.replace_extension(".xml");
    }
    if (cmd.HasArgument("baseAddress")) {
        auto baseAddrStr = cmd.ToLower(cmd.GetArgumentString("baseAddress"));
        if (baseAddrStr == "fm13") {
            BASE_ADDRESS = 0x10638050;
            BASE_MODULE = "GfxCore.dll";
        }
        else if (baseAddrStr == "fifa10") {
            BASE_ADDRESS = 0x9C8868;
            BASE_MODULE = "FIFA10.exe";
        }
        else if (baseAddrStr == "fifa08") {
            BASE_ADDRESS = 0x940030;
            BASE_MODULE = "fifa08.exe";
        }
        else if (baseAddrStr == "fifa07") {
            BASE_ADDRESS = 0x8BE030;
            BASE_MODULE = "fifa07.exe";
        }
        else if (baseAddrStr == "wc06") {
            BASE_ADDRESS = 0x8201F8;
            BASE_MODULE = "FIFAWC06.exe";
        }
        else if (baseAddrStr == "euro08") {
            BASE_ADDRESS = 0x8D0C30;
            BASE_MODULE = "EURO08.exe";
        }
        else
            BASE_ADDRESS = cmd.GetArgumentInt("baseAddress", 0);
    }
    if (is_directory(in)) {
        for (auto const &i : directory_iterator(in)) {
            if (is_regular_file(i.path()) && cmd.ToLower(i.path().extension().string()) == ".cs")
                decompile(i.path(), out / (i.path().stem().string() + ".xml"));
        }
    }
    else
        decompile(in, out);
#endif
#ifdef SCRIPT_DECOMPILER
    CommandLine cmd(argc, argv, { "i", "o" }, {});
    path in, out;
    if (cmd.HasArgument("i"))
        in = cmd.GetArgumentString("i");
    if (cmd.HasArgument("o"))
        out = cmd.GetArgumentString("o");
    if (in.empty())
        in = current_path();
    if (is_directory(in) != is_directory(out))
        out.clear();
    if (out.empty()) {
        out = in;
        if (!is_directory(in))
            out.replace_extension(".xml");
    }
    if (is_directory(in)) {
        for (auto const &i : directory_iterator(in)) {
            if (is_regular_file(i.path()) && cmd.ToLower(i.path().extension().string()) == ".bin")
                decompileScriptBin(i.path(), out / (i.path().stem().string() + ".xml"));
        }
    }
    else
        decompileScriptBin(in, out);
#endif
}
